
function distance(first, second){
	 
	//daca unul dintre param nu e Array, se arunca InvalidType
	if(!Array.isArray(first) || !Array.isArray(second)){
				
			throw new TypeError("InvalidType");
				
	}

	//ret 0 la compararea a doua array-uri vide
	if (first.length==0 || second.length==0){
		return 0;
	}

	//elimminam duplicatele
	const ArrayF1=new Set(first)
	first=Array.from(ArrayF1)

	const ArrayS2=new Set(second)
	second=Array.from(ArrayS2)

	//ret rez corect pt doua array-uri

	var nr=0;
	for(var i=0;i<first.length;i++){
		for(var j=0;j<second.length;j++){
			if(first[i]!=second[j]){
				nr++;
			}

			if(first[i]!==second[j]&&first[i]==second[j]){
				nr+=2;
			}
		}
	}
	return nr;

	
	//TODO: implementați funcția
	// TODO: implement the function
	//cate elem diferite intre siruri
	
}
module.exports.distance = distance


